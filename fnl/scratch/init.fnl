(fn toggle [opts]
  (vim.print "toggled fennel scratch buffer"))

(fn setup []
  (let [cmd (fn [name func] (vim.api.nvim_create_user_command name func {}))]
    (cmd :Scratch toggle)))

{: setup}

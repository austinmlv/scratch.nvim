(when vim.g.loaded_scratch
  (lua "return"))

(set vim.g.loaded_scratch true)

(fn nmap [lhs rhs opts]
  (vim.keymap.set :n lhs rhs opts))

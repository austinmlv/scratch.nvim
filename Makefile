.PHONY: compile clean

compile:
	nvim --headless -u NONE \
		-c "let &runtimepath = &runtimepath . ',deps,' . getcwd()" \
		-c "lua package.path = package.path .. ';$(pwd)/lua/?.lua;deps/?.lua'" \
		-c "lua require('deps.fennel').install().dofile('./deps/build.fnl')" \
		+q

clean:
	rm -r lua/*

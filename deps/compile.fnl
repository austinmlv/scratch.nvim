(local fennel (require :fennel))

(fn slurp [file]
  "Reads all lines from FILE into one string and returns it."
  (with-open [f (io.open file :r)]
    (f:read "*a")))

(fn spit [file string]
  "Writes the contents of STRING into FILE."
  (with-open [f (io.open file :w)]
    (f:write string)))

(fn compile-string [s]
  "Compiles string S and returns either (true result) or (false error)"
  (xpcall
    (fn []
      (fennel.compileString s))
    fennel.traceback))

(fn compile-file [source output]
  (match (compile-string (slurp source))
    (false err) (vim.api.nvim_err_writeln err)
    (true result) (spit output result)))

(fn glob-files [src-dir pattern]
  (vim.fn.globpath src-dir pattern true true))

(fn compile-directory [input-dir output-dir pattern]
  (let [sources (glob-files input-dir pattern)
        targets (-> (vim.iter sources)
                    (: :map (fn [path]
                              (let [path (path:sub (+ 1 (length input-dir)))]
                                (.. output-dir
                                    (path:gsub ".fnl$" ".lua")))))
                    (: :totable))]

    (each [idx path (ipairs targets)]
      (vim.fn.mkdir (vim.fs.dirname path) "p")
      (compile-file (. sources idx) path))))

{: compile-directory
 : compile-file
 : compile-string}

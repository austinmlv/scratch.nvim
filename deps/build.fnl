(local compiler (require :deps.compile))

(vim.api.nvim_out_write "Compiling...\n")
(compiler.compile-directory "fnl" "lua" "**/*.fnl")
(compiler.compile-file "plugin/scratch.fnl" "plugin/scratch.lua")
